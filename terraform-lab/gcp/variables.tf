variable "project"
{
    default = "jp-dev-chaos"
}

variable "machine_type"
{
    default = "f1-micro"
}

variable "zone"
{
    default = "us-central-1a"
}

variable "image"
{
    default = "debian-cloud/debian-10"
}