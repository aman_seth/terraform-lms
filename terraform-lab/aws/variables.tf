variable "aws_region"
{
    default = "ap-south-1"
}

variable "aws_profile_name"
{
    default = "aws-test-mfa"
}

variable "instance_type"
{
    default = t2.micro
}