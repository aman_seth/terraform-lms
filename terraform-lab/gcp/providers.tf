provider "google" {
  credentials = "prod.json"
  project     = var.project_id
}
provider "google-beta" {
  project     = var.project_id
  credentials = "prod.json"
}